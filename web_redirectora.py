#!/usr/bin/python


import socket

response = "HTTP/1.1 200 OK\r\n\r\n" \
		+ "<html><body><h1>Redirigiendo a https://www.google.es</h1></body></html>" \
		+ "\r\n"

response = "HTTP/1.1 302 Found\r\n" \
        "Location: https://www.google.com\r\n" \
        "\r\n"

# Create a TCP objet socket and bind it to a port
# We bind to 'localhost', therefore only accepts connections from the
# same machine
# Port should be 80, but since it needs root privileges,
# let's use one above 1024

mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.bind(('localhost', 1234))

# Queue a maximum of 5 TCP connection requests

mySocket.listen(5)

# Accept connections, read incoming data, and answer back an HTLM page
#  (in a loop)

while True:
    print("Waiting for connections")
    (recvSocket, address) = mySocket.accept()
    print("HTTP request received:")
    received = recvSocket.recv(2048)
    print(received)
    recvSocket.send(response.encode('utf-8'))
    recvSocket.close()
